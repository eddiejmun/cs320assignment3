extern "C"{
  #include "lua.h"
  #include "lualib.h"
  #include "lauxlib.h"
}

int main (int argc, char *argv[])
{
printf("Assignment #3-1, Edward Mun, eddiejmun@gmail.com\n");
    lua_State *world = luaL_newstate();

    luaL_openlibs(world);

    luaL_dofile(world,argv[1]);

    lua_close(world);

    return 0;
}
