function InfixToPostfix(str)
print("Assignment #3-2, Edward Mun, eddiejmun@gmail.com")

symbols = {}
postfix = {}

for x in string.gmatch(str, "%S+") do


    if(x == "*" or x == "/" or x == "%" or x == "+" or x == "-") then

        if (symbols[1] == nil) then
          --insert
          table.insert(symbols,1,x)
   
        --checks if its a minus or plus
        elseif (x == "+" or x == "-") then
          --pop(remove) everything then insert current c(work)
          pop = table.remove(symbols,1)
          table.insert(postfix, pop)
          table.insert(symbols,x)    
          --#symbols = number of elements in table

        -- for * % /
        else
            --check symbol[] if same presedence. pop at 0. insert c(work) to symbol
            if (symbols[1] == "*" or symbols[1] == "/" or symbols[1] == "%") then
              pop = table.remove(symbols,1)
              table.insert(postfix, pop)
              table.insert(symbols,1,x)
            --check if more than 1 symbol in symbol. pop top. insert ^^^
            elseif (#symbols > 1) then
              pop = table.remove(symbols,#symbols)
              table.insert(postfix,pop)
              table.insert(symbols,1,x)
            --else insert to ops
            else
              table.insert(symbols,x)
            end
        end
    else
        --add postfix(numbers)
        table.insert(postfix,x)
    end
end

    --while pop all symbol into postfix
    while (symbols[1]) do
        table.insert(postfix,table.remove(symbols))
    end
    --puts into a string
    final = table.concat(postfix, " ")    
    return final
end