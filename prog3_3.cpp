#include <iostream>
#include<cstring>
extern "C"{
  #include "lua.h"
  #include "lualib.h"
  #include "lauxlib.h"
}

using namespace std;

int main (int argc, char *argv[])
{
printf("Assignment #3-3, Edward Mun, eddiejmun@gmail.com\n");
    string input;

    lua_State *world = luaL_newstate();

    luaL_openlibs(world);

    luaL_dofile(world,argv[1]);

    //lua_getglobal(world,"InfixToPostfix");

    getline (cin,input);

    string test = "return InfixToPostfix('"+input+"')";
    luaL_dostring(world,test.c_str());
    //lua_pushstring(world, input.c_str());

    //lua_pcall(world,1,1,0);
//   cout << luaL_checkstring(world,1);
   cout << "C: " << luaL_checkstring(world,1) << "\n";

    lua_close(world);

    return 0;
}
